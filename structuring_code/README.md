# Testing, Refactoring, Structuring Code and Ideas

## PyTest

Pytest references:
- https://docs.pytest.org/
- https://docs.pytest.org/en/latest/contents.html


Some important command line options:
- `-x`: run only until first failure
- `-k`: select tests by name
- `--pdb`: enter debugger on failure
- `--duration=0`: measure how long tests run
- `--doctest-modules`: to run doctests as well
- `--cov`: to compute test coverage
- `--cov --cov-report=html`: to create annotated html
- ...

And many others, make sure you go through the documentation.
