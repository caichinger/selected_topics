# AirBase Station Data

**Exercise**

Implement functionality to simplify access to the data provided in the
`./data/` directory.
`./data/raw` contains raw data as provided by the EEA data service whereas
`./data/preprocessed` contains the data in an already preprocessed form.

Hints:
- Take a look at `raw` to get a feeling for the data.
- Start to work with the preprocessed data.

Optional:
- Implement functionality to derive `preprocessed` from `raw`.
- Implement functionality to analyse the data using your utilities.
