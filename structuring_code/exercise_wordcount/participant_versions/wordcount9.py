#import re
def word_count(texts):
    cnt_dict = dict()
    for text in texts:
        text = text.lower()
        # TODO more preprocessing removing satzzeichen.. maybe using re package
        words = text.split()
        for word in words:
            n = cnt_dict.setdefault(word,0)
            cnt_dict[word] = n + 1
    return cnt_dict
