#!/usr/bin/env python
# -*- coding: utf-8 -*-

def show_word_count(text):
	from collections import Counter
	text = text.replace(',', '')
	text = text.replace('.', '')
	worter=text.split()
	d=Counter(worter)
	for key, value in d.iteritems():
    	print "%-30s  %4d" % (key, value)

text1="""Und jetzt ist es so weit und wir fangen an und blicken weit in die Ferne"""
text2="""Um Soziale Sicherheit, neue Gerechtigkeit und Armutsbekämpfung kümmern sich für die ÖVP Klubobmann August Wöginger und für die Grünen die Wiener Landesparteichefin Birgit Hebein. Ex-Ministerin Margarete Schramböck und die Grüne Sigrid Maurer verhandeln den Bereich Bildung, Wissenschaft, Forschung und Digitalisierung."""
texte=[text1, text2]
for text in texte:
	show_word_count(text)
