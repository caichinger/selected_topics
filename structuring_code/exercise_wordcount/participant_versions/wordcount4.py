import re

test_text1 = "dog cat girl boy, austria hungary germany hug. cry girl cat cat cat"
test_text2 = "usa vienna Budapest budapest Get over here half life usa vienne vienna budapest"
all_text = [test_text1, test_text2]

def count_unique(input_text):
    all_words = []
    for i in input_text:
        splitted_text = i.split(' ')
        all_words.extend(splitted_text)

    text_container = {}
    for w in all_words:
        w = w.lower()
        w = re.sub(r'[^\w\s]','',w)
        if w not in text_container.keys():
            text_container.update({w: 1})
        else:
            text_container[w] += 1

    return text_container

count_unique(all_text)
