from collections import Counter
import string

def counter(text):
    return Counter(text.translate(str.maketrans('', '', string.punctuation)).lower().split())

texts = ['hello I am test - Hello, hello Hello', 'a A, abc . # A']
text =' '.join(texts)

c = counter(text)
print(c)
