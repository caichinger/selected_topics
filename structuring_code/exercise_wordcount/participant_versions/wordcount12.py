from string import ascii_letters
import doctest

txt1='hello this is a text'
txt2='hello this is also a text'
txt3="""This is a slightly more complicated text. It's not just lower case"""

def count_words(*texts):
    """
    Count words in a string.

    Parameters
    txts : multiple strings

    Returns:
    --------
    wdict : dictionary

    Example
    -------
    >>> count_words('This is a test so let us test this')
    {'this': 2, 'is': 1, 'a': 1, 'test': 2, 'so': 1, 'let': 1, 'us': 1}
    """
    wdict = {}
    for txt in texts:
        allowed = set(ascii_letters + ' '+ '\'')
        txt_clean = ''.join(l for l in txt if l in allowed)
        txt_lst=txt_clean.lower().split()
        for word in txt_lst:
            if word in wdict.keys():
                wdict[word] = wdict[word]+1
            else:
                wdict[word]=1
    return wdict

count_words(txt1, txt2, txt3)

doctest.testmod()
