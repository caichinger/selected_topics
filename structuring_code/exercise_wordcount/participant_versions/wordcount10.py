def count_word(text,*args,match_word=None):
    import re
    if len(args) > 0:
        text = text + ' ' + ' '.join(args)

    words=re.findall(r"[\w']+",text)
    if not match_word:
        a={}
        for i in set(words):
            a.update({i:sum([word == i for word in words])})
        return a
    return sum([word == match_word for word in words])
