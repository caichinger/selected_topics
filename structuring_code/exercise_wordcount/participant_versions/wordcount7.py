def word_frequencies(my_text):
    from collections import defaultdict

    my_output = defaultdict(list)

    i=0
    for word in my_text.lower().replace(',','').replace('.','').split():
        key = word
        # my_output[key] += 1
        if key not in my_output:
            my_output[key] = 1
        else:
            my_output[key] += 1

    for key, count in my_output.items():
        print(key, count)
