
import this

test_text1 = """The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!"""

test_text2 = """Wenn Kolosse wie Giraffen oder Gaure das obere Ende der
Paarhufer-Vielfalt markieren, dann bildet die Gruppe der Hirschferkel, zu
denen auch der Vietnam-Kantschil gehört, die Untergrenze. Die zierlich gebauten
Hirschferkel sind nur etwa so groß wie Feldhasen oder Katzen. Früher waren sie in
der ganzen Alten Welt verbreitet, heute findet man die unscheinbaren Tiere nur
noch in den tropischen Regenwäldern Afrikas und Asiens."""

test_text3 = """Ebenfalls bestätigt wurde, dass es bei der Hausdurchsuchung um die
Casinos-Austria-Affäre geht. Die ÖBAG hält 33,24 Prozent an der Casinos Austria AG.
Laut STANDARD-Informationen wurde auch die Liste der Beschuldigten deutlich ausgeweitet.
Darauf befinden sich nun Löger, Ex-Vizekanzler Josef Pröll (ÖVP), der stellvertretender
Aufsichtsratspräsident bei den Casinos ist, sowie Casinos-Aufsichtsratspräsident Walter
Rothensteiner und ÖBAG-Chef Thomas Schmid. Schmid war unter Löger Generalsekretär im
Finanzministerium. Die ÖBAG betont, dass sie nicht in die Vorstandsbestellung des
Ex-FPÖ-Bezirksrats Peter Sidlo eingebunden gewesen sei.

Davor wurde schon gegen die früheren FPÖ-Granden Heinz-Christian Strache und Johann
Gudenus sowie Verantwortliche der Novomatic ermittelt. Alle Genannten haben die Vorwürfe
bisher energisch zurückgewiesen, es gilt die Unschuldsvermutung."""

def text_cleaning_to_list(text):
    if type(text) == list:
        clean_list = []
        for entry in text:
            entry = entry.replace('\n', ' ')
            entry = entry.replace('\t', ' ')
            entry = entry.replace('--', ' ')
            entry = entry.replace('-', ' ')
            entry = entry.replace('!', ' ')
            entry = entry.replace('*', ' ')
            entry = entry.replace('.', ' ')
            entry = entry.replace(',', ' ')
            entry = entry.replace(')', ' ')
            entry = entry.replace('(', ' ')
            clean_list.append(entry.split(' '))
        clean = []
        for list_ in clean_list:
            for word in list_:
                clean.append(word)

    else:
        text = text.replace('\n', ' ')
        text = text.replace('\t', ' ')
        text = text.replace('--', ' ')
        text = text.replace('-', ' ')
        text = text.replace('!', ' ')
        text = text.replace('*', ' ')
        text = text.replace('.', ' ')
        text = text.replace(',', ' ')
        text = text.replace(')', ' ')
        text = text.replace('(', ' ')
        clean = text.split(' ')
    return clean

from collections import defaultdict
def word_count(text):
    words = set()
    words_freq = defaultdict(int)
    for i, word in enumerate(text):
        if word not in '':
            words.add(word)
    for i, word in enumerate(words):
        words_freq[word] = text.count(word)
    return words_freq

text_clean = text_cleaning_to_list([test_text1, test_text2, test_text3])
test = word_count(text_clean)
print(test)
