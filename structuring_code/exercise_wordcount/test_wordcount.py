"""
TODO:
(preprocessing('Hi!'),
tokenize('Ha ha'),
count_frequencies([1, 1]),
word_count_for_single_text('a. A'))

('hi!', ['Ha', 'ha'], {1: 2}, {'a': 2})

d1 = {'a': 1}
d2 = {'a': 1, 'b': 1}

total = {'a': 2, 'b': 1}

merge_word_counts(d1, d2)
"""
from wordcount import word_count

def test_word_count():
    assert word_count(['a', 'a', 'a.', 'b']) == {'a': 3, 'b': 1}


def test_word_count_with_empty_data():
    assert word_count([]) == {}

#def test_word_count_with_empty_data():
#    assert word_count([1]) == {}
