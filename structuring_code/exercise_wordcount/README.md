# Word Count

Implement functionality to compute word frequencies given several texts.


Run tests using
- `pytest`
- `pytest --cov`
- `pytest --cov --cor-report=html`
- `ptw`  (pytest watch, automatic test runner)