"""
Wordcount exercise

Suggested issues:
- How to implement input sanitization?
- How to make sure the `word_count` *always* returns *something*?
- How about parallelization?
- How to deal with the requirement "input texts as file glob pattern"?

Aspects we discussed:
If we parallelize, ideally we want to do something like
```
from multiprocessing import Pool
pool = Pool()
pool.map(word_count_for_single_text, texts)
```
hence we face the question at which point we wish to perform input
sanitization (on loading the texts or on processing?).
Note that generators are a way to compile (lazy) processing pipelines.
Note that one wants to keep the data that needs to be copied from
one process to another small, hence if we were reading data from files,
it would make sense to e.g. pass only file paths to each worker job.
It is possible to share memory between processes but the less alignment
we require, the better.
"""
from collections import Counter


def word_count(texts):
    total_word_counts = []
    # note the difference between the two
    # the latter is a generator expression and hence lazy
    # valid_texts = [text for text in texts if isinstance(text, str)]
    valid_texts = (text for text in texts if isinstance(text, str))
    for text in valid_texts:
        # check if text is actually a text
        # inside the for loop or outside? as part of a function or not?
        # if not isinstance(text, str):
        #     continue
        word_counts = word_count_for_single_text(text)
        total_word_counts.append(word_counts)
    return merge_word_counts(*total_word_counts)


def word_count_for_single_text(text):
    text = preprocessing(text)
    words = tokenize(text)
    word_counts = count_frequencies(words)
    return word_counts


def preprocessing(text):
    return text.lower().replace('.', '')


def tokenize(text):
    return text.split(' ')


def count_frequencies(words):
    frequencies = Counter(words)
    return frequencies


def merge_word_counts(*word_counts):
    total = {}
    for word_count in word_counts:
        for word, count in word_count.items():
            if word not in total:
                total[word] = count
            else:
                total[word] += count
    return total
