"""
Pytest example tests.
"""
import pytest

from demo import simple_multiply, valmap


def test_mulitply():
    assert simple_multiply(1, 2) == 2


def test_valmap_raises_value_error_if_not_mapping_passed():
    with pytest.raises(ValueError):  # this is a context manager
        valmap('this does not matter', 'something that is not a mapping')


@pytest.mark.parametrize('function, data, expected',
[
    (lambda x: x, {}, {}),
    (lambda x: x, {'a': 1}, {'a': 1}),
    (lambda x: x+1, {'a': 1}, {'a': 2}),
])
def test_valmap_with_different_inputs(function, data, expected):
    assert valmap(function, data) == expected
