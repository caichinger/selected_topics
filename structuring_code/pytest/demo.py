"""
Test example functions.

Add (py)tests to below code snippets (cf. pytest slides).
"""
import pytest


def simple_multiply(a, b):
    return a * b


"""
Possible names include:
- checkdict
- dict_funct
- multiply_dict_values
- apply_function_to_dict
- valmap
"""
def valmap(function, data):
    if not isinstance(data, dict):  # TODO: isinstance should be replaced
        raise ValueError('`data` needs to be of type dict')
    new_data = {}
    for key, value in data.items():
        new_data[key] = function(value)
    # return {key: function(value) for key, value in data.items()}
    return new_data


def raises_zero_division_error():
    # write a test for this one
    raise ZeroDivisionError('Beware the zero division error')


def super_slow_function():
    # mark and exclude
    from time import sleep
    sleep(10)


def test_not_yet_implemented_stuff():
    # imagine we cannot run this code because it does not exist yet
    pass


def test_something_that_you_cannot_run():
    # imagine we cannot run this code because we are on the wrong platform
    pass


def add_one_to_array(x):
    return x + 1  # do you forsee any issues?


def add_column_to_dataframe(df):
    df['new_column'] = 1  # do you forsee any issues?
    return df
