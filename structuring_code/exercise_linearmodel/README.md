**Exercises**

*The accompanying notebook provides additional context (and the solution 
when using already existing tools).*


Given a/n DataFrame/array:

1) Normalization
Implement functionality to normalize columns.


2) Polynomial Features
Implement functionality to compute polynomial terms 
(including interactions, i.e. products) up to a certain `order`.


3) Linear Regression
Implement functionality to solve the [least squares](https://en.wikipedia.org/wiki/Linear_least_squares) problem related to

$${\displaystyle \mathbf {y} =X{\boldsymbol {\beta }}+{\boldsymbol {\varepsilon }},\,}$$

i.e. compute

$${\displaystyle {\hat {\boldsymbol {\beta }}}=(\mathbf {X} ^{\mathsf {T}}\mathbf {X} )^{-1}\mathbf {X} ^{\mathsf {T}}\mathbf {y} .}$$


This corresponds to fitting a [linear regression](https://en.wikipedia.org/wiki/Linear_regression) model.


4) Rework your implementations to conform to the [scikit-learn](https://scikit-learn.org/stable/) interfaces of a
- [Transfomer](https://scikit-learn.org/stable/modules/generated/sklearn.base.TransformerMixin.html#sklearn.base.TransformerMixin)
- [Regressor](https://scikit-learn.org/stable/modules/generated/sklearn.base.RegressorMixin.html#sklearn.base.RegressorMixin)

5) Combine your tools using a scikit-learn [pipeline](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html#sklearn.pipeline.Pipeline) of the form
  1. Normalize
  1. Polynomial Features
  1. Linear Regression
