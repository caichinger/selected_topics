"""
Doctest Exercises.

Implement a doctest for below code snippets.
"""
import matplotlib.pyplot as plt


class SomeClass:
    # implement proper __repr__
    def __init__(self, foo):
        self.foo = foo


def fancy_plot():
    fig, ax = plt.subplots()
    ax.plot([1, 2])
    return ax


def possibly_unordered_return_value():
    return {'b': 2, 'a': 1}


def many_digit_number():
    return 1/42


def some_exception():
    raise ZeroDivisionError('O.o')


def multiply(a, b):
    """
    Computes a * b

    >>> multiply(['A', 'B'], 2)
    ['A', 'B',
     'A', 'B']
    """
    return a * b
