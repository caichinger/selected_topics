# Cheatsheets

- Python (a cheatsheet for Python is provided in the lecture folder)
- [Jupyter](https://www.cheatography.com/weidadeyue/cheat-sheets/jupyter-notebook/)
- VSCode
  - [Linux](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-linux.pdf)
  - [Windows](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf)
