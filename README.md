# Selected Advanced Topics

A customized course for ZAMG by Claus Aichinger (claus.aichinger@gmail.com).

This work is licensed under the 
Creative Commons Attribution 4.0 International License. 
To view a copy of this license, visit 
http://creativecommons.org/licenses/by/4.0/.

## System

- Login with your domain account (active directory)
- Start **MobaXterm** using the existing profile
- Login with workshop user and password


## Technical Setup

Please follow below description to set up your working environment.

This is what we do:
1. Create a directory for our code
2. Use `git` to retrieve the workshop material
  1. `clone` to create a local copy
  2. `checkout` to create a personal branch to be later able to `pull` 
     upstream changes, i.e. updates in the lecture notes, without 
     interferring with your personal changes
3. Use `conda` to create a Python environment
4. Open a coding environment

If you do not want to use `git`, you can download the repository as a zip
file as well (see below).

### Using Git

First session:

```bash
# create a <directory where you work on code>
cd <directory where you work on code>
git clone <repository path> <project name>
cd <project name>
git checkout -b <branch name>
conda env create -f env.yml
```

Second session:

```bash
cd <project path>
git add <changes you wish to add>
git commit -m"<>"
ǵit checkout master
git pull
# option 1
git checkout <branch name>
git merge master
# option 2
git checkout <new branch name>
```


### Without Git

First session:

```bash
#  download and extract the repository zip file
cd <project path>
conda env create -f env.yml
```

Second session:

```bash
#  download and extract the repository zip file
```


## Coding Environment

```bash
cd <project path>
conda activate TODO
# Option 1
jupyter notebook
# Option 2
code .
```


## Important Links

### Share code snippets with colleagues

[HERE](https://docs.google.com/document/d/1Zc1HTRlusL7LLl0802A-tS3SDSv-ZGZKVT8bzFbivHA/edit?usp=sharing)


### Follow up questions

[HERE](https://forms.gle/Z779TSQxMzpf9Fk67)

*After the first day, you are invited to submit questions through this form.*


### Feedback

[HERE](https://forms.gle/sDLdR9MrKveAb7k28)

*After the second day, you are invited to provide feedback through this form.*
